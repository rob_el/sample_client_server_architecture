from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
from http import HTTPStatus

class HTTP_Request_Handler(BaseHTTPRequestHandler):

    def __set_headers(self):
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_HEAD(self):
        self.__set_headers()

    def do_GET(self):
        rootdir = 'D:/xampp/htdocs/'
        self.do_HEAD()

        try:
            if self.path.endswith('.html'):
                open_file = open(rootdir + self.path)
                self.wfile.write(bytes(open_file.read(),"utf-8"))
                open_file.close()
            return
        except IOError:
            self.send_error(404, 'file not found')

def run(server_class=HTTPServer, request_handler_class=HTTP_Request_Handler):
    server_address = ('localhost', 800)
    httpd = server_class(server_address, request_handler_class)
    try:
        print("Server works on http://localhost:800")
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("Stop the server on http://localhost:8000")
        httpd.socket.close()

if __name__ == '__main__':
    run()
